/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newshop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class UiService {
   private static ArrayList<Product> ProductList = new ArrayList<>();
    static {
        ProductList.add(new Product("1", "Rice", "easy", 15.0, 1));
        ProductList.add(new Product("2", "Sausage", "Ceepee", 35.0, 1));
        ProductList.add(new Product("3", "Snack", "ArhanYodkhun", 10.5, 2));
    }

    public static boolean addOrder(Product order) {
        ProductList.add(order);
        save();
        return true;
    }

    public static boolean delOrder(Product order) {
        ProductList.remove(order);
        save();
        return true;
    }

    public static boolean delOrder(int index) {
        ProductList.remove(index);
        save();
        return true;
    }

    public static ArrayList<Product> getOrder() {
        return ProductList;
    }

    public static Product getOrder(int index) {
        return ProductList.get(index);
    }

    public static boolean updateOrder(int index, Product order) {
        ProductList.set(index, order);
        save();
        return true;
    }

    public static boolean clear() {
        ProductList.removeAll(ProductList);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("New.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ProductList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductSevice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductSevice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("New.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ProductList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductSevice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductSevice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductSevice.class.getName()).log(Level.SEVERE, null, ex);
        }
    
}}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.newshop;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Product implements Serializable {

    private String Id;
    private String Name;
    private String Brand;
    private double Price;
    private int Amount;

    public Product(String Id, String Name, String Brand, double Price, int Amount) {
        this.Id = Id;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = Amount;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    @Override
    public String toString() {
        return "Product" + "Id=" + Id + ", Name=" + Name
                + ", Brand=" + Brand + ", Price=" + Price
                + ", Amount=" + Amount ;
    }

}
